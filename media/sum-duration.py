#!/bin/python
import sys
import os,subprocess

files = {}

for file in sys.argv[1:]:
    try:   
        duration = float(subprocess.Popen(['ffprobe', '-v', 'error', '-show_entries',
            'format=duration', '-of', 'default=noprint_wrappers=1:nokey=1', file],
            stdout=subprocess.PIPE).stdout.read().decode("utf-8").rstrip())
            # duration in seconds, decoded as utf-8 and newline removed and converted to float
    except ValueError:
        pass
    files[file] = duration
    
files["sum"] = sum(files.values())
    
"""def getKey(item):
    return float(item[1]) # Sort after bitrate which is the second entry"""

def getKey(item):
    return item[0]

sorted_files = sorted(files.items(), key=getKey)

for x in sorted_files:
    print("Duration: %10.0f file: %s" % (x[1], x[0]))
