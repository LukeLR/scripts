#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
void print_array(double* A, int n, int offset){
	for (int i = 0; i < n; i++){
		printf("%2i: %2.0lf (%i)\n", i, A[i], ((i - offset) % n + n) % n);
	}
}

void rotate_array(double *A, int n, int offset){
	int start = 0;
	int cur = 0;
	int next = 0;
	double cur_temp = A[cur];
	double next_temp = 0.0;
	
	for (int i = 0; i < n; i++){
		next = (cur + offset) % n;
		next_temp = A[next];
		A[next] = cur_temp;
		
		cur = next;
		cur_temp = next_temp;
		if (cur == start){
			printf("Reached %d again. Starting new cycle from %d.\n", start, start + 1);
			start++;
			cur = start;
			cur_temp = A[cur];
		}
	}
}

int main(int argc, char** argv){
	int rotation = atoi(argv[1]);
	int n = argc - 2;
	double* A = malloc(n * sizeof(double));
	for (int i = 0; i < n; i++){
		sscanf(argv[i + 2], "%lf", A + i);
	}
	print_array(A, n, rotation);
	rotate_array(A, n, rotation);
	printf("Rotated by %d:\n", rotation);
	print_array(A, n, rotation);
}

/*k*offset \congruent 0 (mod n)16:53
gcd(k*offset,n)=n16:54
(start + k*offset) % n16:54
offset=4, n=1016:56
start=016:56
0, 4, 8, 2, 6, 016:57
1, 5, 9, 3, 7, 116:57
gcd(4,10)=2 */
