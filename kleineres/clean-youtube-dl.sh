#!/bin/bash
mkdir annotations
mv *.xml annotations/
mkdir descriptions
mv *.description descriptions/
mkdir infos
mv *.json infos/
mkdir thumbnails
mv *.jpg thumbnails/
