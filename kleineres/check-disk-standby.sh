if [ $1 ]; then
echo '########################################'
echo '###### Advanced Power Management #######'
echo '########################################'
for i in /dev/sd?; do
	sudo hdparm -B $i
done

echo '########################################'
echo '###### Standby Spindown Value ##########'
echo '########################################'
for i in /dev/sd?; do
	echo $i
	sudo hdparm -S $i
done

echo '########################################'
echo '#### Automatic Acoustic Management #####'
echo '########################################'
for i in /dev/sd?; do
	echo $i
	sudo hdparm -M $i
done

echo '########################################'
echo '###### Currently in Standby Mode? ######'
echo '########################################'
for i in /dev/sd?; do
	echo $i
	sudo smartctl -i -n standby $i
done
else
for i in /dev/sd?; do
	echo -n "$i ("
	lsblk --output SIZE --nodeps --noheadings $i | tr -d '\n'
	echo -n "): "
	find /dev/disk/by-path -name "*-usb-*" -not -name "*-part*" -exec readlink -f {} \;|grep $i > /dev/null
	if [ $? -eq 0 ]; then
		echo "USB, won't probe."
	else
		sudo smartctl -i -n standby $i>/dev/null
		if [ $? -eq 2 ]; then
			echo "Standby"
		else
			echo "Power"
		fi
	fi
done
fi

