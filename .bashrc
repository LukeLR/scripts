# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#as in https://unix.stackexchange.com/questions/12107/how-to-unfreeze-after-accidentally-pressing-ctrl-s-in-a-terminal
stty -ixon # disable terminal freezing by ctrl+s etc.

alias ls='ls --color=auto'
PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
#PS1='[\u@\h \W]\$ '

HISTSIZE=-1
HISTFILESIZE=-1
HISTTIMEFORMAT="%d.%m.%y %T "
HISTCONTROL=ignoreboth
shopt -s histappend

alias ll='ls -ahl'
alias catn='/usr/bin/cat --number'
#alias nano='nano --softwrap -i'
#alias cat='cat --number'
#alias catn='/usr/bin/cat'
alias recodeutf8='recode ISO8859-14..UTF-8 *'
PATH=$PATH:/home/lukas/bin

# added by travis gem
[ -f /home/lukas/.travis/travis.sh ] && source /home/lukas/.travis/travis.sh

# added for "fuck"-command
# eval $(thefuck --alias)

export EDITOR=nano;
#SSH_AUTH_SOCK=/tmp/ssh-p48dGFiEMJbz/agent.2135; export SSH_AUTH_SOCK;
#SSH_AGENT_PID=2136; export SSH_AGENT_PID;
#echo Agent pid 2136;
