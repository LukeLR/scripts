#!/bin/bash
file="$1"
echo $file
ending=.mkv
profile=RF23-mkv-AutoPassVorb-AutoSubtitle-Crop-
appendix=_$profile
outfile=$(basename "$file" "$ending")$appendix$ending
dir=./done/$(dirname "$file")
outdir="$dir/encoded/"
finisheddir="$dir/finished/"

mkdir -p "$outdir"
mkdir -p "$finisheddir"
handbrakecli --preset-import-gui $profile -Z $profile -i "$file" -o "$outdir$outfile" && mv "$file" "$finisheddir$(basename "$file")"
