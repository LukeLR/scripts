# scripts
Scripts I use in other projects or in daily use

## Contents
#### check-string-in-file.lua
a small lua script that provides methods for a) reading text from a text file and b) checking if a given string is included in that text file. Written to provide [PacoLM](../../../../PacoLM) in [vysheng/tg #720](../../../../vysheng/tg/issues/720) with a method to filter users. I used a similiar method for my telegram bot implementation before I [completely switched to Java](../../../RaPiTelegram)
